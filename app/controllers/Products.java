package controllers;
import play.mvc.Controller;
import play.mvc.Result;
//import java.util.List;
//import java.util.ArrayList;
import models.Product;
import models.Tag;
import models.StockItem;
import views.html.products.list;
import play.data.Form;
import views.html.products.details;
import java.util.*;
import com.avaje.ebean.Page;
public class Products extends Controller {
	private static final Form<Product> productForm = Form.form(Product.class);
	//productForm = productForm.fill(product);
	///

	public static Result list(){
		List<Product> products = Product.findAll();
	    return ok(list.render(products));
	}
	public static Result newProduct(){
		return ok(details.render(productForm));
	}
	public static Result details(String ean){
		final Product product = Product.findByEan(ean);
		if (product == null) {
			return notFound(String.format("Product %s does not exist.", ean));
		}
		Form<Product> filledForm = productForm.fill(product);
		return ok(details.render(filledForm));
	}
	public static Result save(){
		/*Form<Product> boundForm = productForm.bindFromRequest();
		Product product = boundForm.get();
		product.save();
		return ok(String.format("Saved product %s", product));
		*/ //v1
		
		Form<Product> boundForm = productForm.bindFromRequest();
		if (boundForm.hasErrors()) {
		flash("error", "Please correct the form below.");
		return badRequest(details.render(boundForm));
		}
		Product product = boundForm.get();
		//Ebean.save(product);
		
		List<Tag> tags = new ArrayList<Tag>();
		for (Tag tag : product.tags) {
			if (tag.id != null) {
				tags.add(Tag.findById(tag.id));
			}
		}
		product.tags = tags;
		StockItem stockItem = new StockItem();
        stockItem.product = product;
        stockItem.quantity = 0L;
       
        product.save();
        stockItem.save();
		
		flash("success", String.format("Successfully added product %s", product));
		return redirect(routes.Products.list());
		//v2
		
		/* //... (binding and error handling)
		Product product = boundForm.get();
		List<Tag> tags = new ArrayList<Tag>();
		for (Tag tag : product.tags) {
			if (tag.id != null) {
				tags.add(Tag.findById(tag.id));
			}
		}
		product.tags = tags;
		product.save();
   // ... (success message and redirect)
		*/
	}
	public static Result delete(String ean) {
		/*final Product product = Product.findByEan(ean);
		if(product == null) {
		return notFound(String.format("Product %s does not exists.", ean));
		}
		Product.remove(product);
		return redirect(routes.Products.list());
		*/ //v1
		
		final Product product = Product.findByEan(ean);
		if(product == null) {
		return notFound(String.format("Product %s does not exists.", ean));
		}
		product.delete();
		return redirect(routes.Products.list());
		
		/*final Product product = Product.findByEan(ean);
        if (product == null) {
            return notFound(String.format("Product %s does not exists.", ean));
        }
        for (StockItem stockItem : product.stockItems) {
            stockItem.delete();
        }
        product.delete();
        return redirect(routes.Products.list(0));*/
	}
}