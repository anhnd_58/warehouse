package models;
import play.data.validation.Constraints;
import java.util.*;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Warehouse extends Model {
    @Id
    public Long id;
 
    public String name;
	@OneToOne
	public Address address;
 
    @OneToMany(mappedBy = "warehouse")
    public List<StockItem> stock = new ArrayList(); // truong quan he
    public String toString() {
        return name;
    }
}
