name := """warehouse"""

version := "1.0-SNAPSHOT"

libraryDependencies += javaEbean

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache,
  javaWs
)
